BEGIN {
	FS = "_"
}

{
	arr[$1] = arr[$1] $2 "; "
}

END {
	for (item in arr) printf "%s: %s\n", item, arr[item]
}
