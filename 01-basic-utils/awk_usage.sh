USAGE="USAGE: awk_usage dir
    dir: directory containing files with english texts"
DIR="$1"

if [ -z "$1" ]
then
	echo "Provide some cli arguments please :)"
	echo "$USAGE"
	exit 1
elif [ -f "$1" ]
then
	echo "Argument must be a directory"
	exit 2
fi

# 1. List all dates per store
# ls -1 --color=never "$DIR" | awk 'BEGIN { FS = "_" } ; { arr[$1] = arr[$1] $2 "; " } ; END { for (item in arr) printf "%s: %s\n", item, arr[item] }'
printf "List all dates per store\n"
ls -1 --color=never "$DIR" | awk -f "$(dirname $0)/list_dates.awk"

# 2. List all stores opened at a specific day
printf "\nList all stores opened at specific date\n"
echo "Date:"
read DATE
ls -1 --color=never "$DIR" | awk -v date="$DATE" -f "$(dirname $0)/stores_at_date.awk"

# 3. Generate summary of sold items per store
printf "\nSummary per store:\n"
# Run manually
# awk -f "$(dirname $0)/total_per_store.awk" "$DIR"

# 4. Print amount of stores with specific goods
printf "\nPrint count of stores with specific goods\n"
echo "Goods:"
read GOODS
# Run manually

