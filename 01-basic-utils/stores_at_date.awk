BEGIN {
	FS = "_"
}

{
	if ($2 == date) arr[$1]
}

END {
	for (item in arr) print item
}
