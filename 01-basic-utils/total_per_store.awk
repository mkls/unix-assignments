BEGIN {
	FS = " "
};

{
	split(FILENAME, arr, "_")
	sub(".*/", "", arr[1])
	total[arr[1]] += $2 * $3
};

END {
	for (store in total)
		printf "%s: total is %d rubles\n", store, total[store]
}
