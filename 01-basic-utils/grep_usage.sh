#!/usr/bin/env sh

USAGE="USAGE: grep_usage dir
    dir: directory containing files with english texts"
DIR="$1"
VOWELS="aeyiuo"

if [ -z "$1" ]
then
	echo "Provide some cli arguments please :)"
	echo "$USAGE"
	exit 1
elif [ -f "$1" ]
then
	echo "Argument must be a directory"
	exit 2
fi

# 1. Print all words (starting with a vowel) in alphabetic order
printf "Words starting with a vowel:\n"
grep "$DIR" -Rihe "^[$VOWELS].*" | sort

# 2. Count total number of words
printf "\nTotal number of words:\n"
grep "$DIR" -Rwie '^[a-z]*' | wc -l

# 3. Print filenames containing lines without vowels
printf "\nFiles containing lines without vowels:\n"
grep "$DIR" -Rlihve "^.*[$VOWELS].*"

# 4. Print last world (in alphabetic order, case-insensitive)
printf "\nLast (alphabetical) word:\n"
grep "$DIR" -Rihe "^[$VOWELS].*" | sort | tail -n 1
