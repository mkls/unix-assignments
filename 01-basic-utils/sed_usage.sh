#!/usr/bin/env sh

USAGE="USAGE: sed_usage file
    file: file to scan and edit"
# sed does not support negative lookahead :(
# REGEX="^(?!(.*error|ERROR)).*"
FILE="$1"
ALPHABET="AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
INVERSED="ZzYyXxWwVvUuTtSsRrQqPpOoNnMmLlKkJjIiHhGgFfEeDdCcBbAa"


if [ -z "$1" ]
then
	echo "Provide some cli arguments please :)"
	echo "$USAGE"
	exit 1
elif [ -d "$1" ]
then
	echo "Argument must be a regular file"
	exit 2
fi

# 1. Print all lines not containing 'error' or 'ERROR'
printf "\n>> Lines without 'error' or 'ERROR':\n"
sed -E '/.*(error|ERROR).*/d' "$FILE"

# 2. Append "Achtung!!!" to the end of every line ending with error or ERROR
printf "\n>> Errors with 'Achtung!!!' appended:\n"
sed -E 's/(.*error.*|.*ERROR.*)/\1\nAchtung\!\!\!/g' "$FILE"

# 3. Replace error line with 'Achtung!!!'
printf "\n>> Replace errors with 'Achtung!!!':\n"
sed -E 's/(.*error.*|.*ERROR.*)/Achtung\!\!\!/g' "$FILE"

# 4. Transliterate error lines
printf "\n>> Transliterating error lines:\n"
sed -E "/.*(error|ERROR).*/y/$ALPHABET/$INVERSED/" "$FILE"
