#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <ncurses.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "ipc.h"
#include "window.h"

#define BUFSIZE 128
#define MAX_MSG 256

int check_fifo(const char* fifo_name, const int mode, const int permissions) {
    int fd;

    mkfifo(fifo_name, permissions);

    fd = open(fifo_name, mode);

    switch (errno) {
        case 0:
        case EEXIST:
            break;
        default:
            printf("ERR: Can't open fifo - %s", strerror(errno));
            fd = -1;
    }

    return fd;
}

int check_set_pid(const char* fname, int base_mode) {
    int pid_fd = open(fname, base_mode | O_CREAT | O_EXCL);

    if (pid_fd != -1) {
        puts("Setting pseudo-lock");
        char* str = (char*)malloc(sizeof(char) * 10);
        snprintf(str, sizeof(char) * 10, "%d", getpid());
        write(pid_fd, str, strlen(str));
        free(str);
    }

    return pid_fd;
}

int main(int argc, char** argv) {
    char* fname1 = "chat_channel_1.fifo";
    char* fname2 = "chat_channel_2.fifo";
    char *rname, *wname;
    const char* fpid = "chat.pid";

    char* buffer = (char*)malloc(sizeof(char) * MAX_MSG);

    int fdr, fdw;
    pthread_t reader_thread;

    setlocale(LC_ALL, "");
    signal(SIGPIPE, SIG_IGN);

    int pid_fd = check_set_pid(fpid, O_RDWR);

    if (pid_fd != -1) {
        rname = fname1;
        wname = fname2;
    } else {
        rname = fname2;
        wname = fname1;
    }

    fdr = check_fifo(rname, O_RDWR | O_NONBLOCK, 0600);
    fdw = check_fifo(wname, O_RDWR | O_NONBLOCK, 0600);

    int ch;

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();

    refresh();

    int msg_list_h = (int)(LINES * 0.9);
    int input_h = LINES - msg_list_h;

    struct window* msg_list = setup_custom_window(0, 1, msg_list_h, COLS - 2);
    struct window* input =
        setup_custom_window(msg_list_h, 1, input_h, COLS - 2);

    scrollok(input->win, true);
    scrollok(msg_list->win, true);

    int pos = 0;

    struct thread_data td;

    td.fd = fdr;
    td.w = msg_list;

    pthread_create(&reader_thread, NULL, &reader, (void*)&td);

    while ((ch = getch()) != 27) {
        if (ch == KEY_BACKSPACE) {
            if (pos != 0) {
                pos--;
            }

            backspace_pressed(input);
        } else if (ch == KEY_ENTER || ch == '\n') {
            buffer[pos] = '\0';
            pos = 0;

            errno = 0;

            write(fdw, buffer, strlen(buffer) + 1);

            if (errno != 0) {
                print_message(msg_list, "Connection terminated: waiting",
                              "system", 30);

                fdw = open(wname, O_WRONLY);

                print_message(msg_list, "Connection reestablished", "system",
                              30);
                write(fdw, buffer, strlen(buffer) + 1);
            }

            print_message(msg_list, buffer, "me", MAX_MSG);
            enter_pressed(input);
        } else {
            if (pos != MAX_MSG - 1) {
                print_char(input, ch, true);
                buffer[pos++] = ch;
            }
        }
    }

    pthread_cancel(reader_thread);

    endwin();

    free(buffer);

    if (pid_fd != -1) {
        close(pid_fd);
        unlink(fpid);
    }

    return 0;
}
