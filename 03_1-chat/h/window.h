#ifndef WINDOW_H
#define WINDOW_H

#include <ncurses.h>

struct window {
    int cx;
    int cy;

    int h;
    int w;

    WINDOW *win;
};

struct window *setup_custom_window(int y, int x, int h, int w);

void backspace_pressed(struct window *win);

void print_message(struct window *win, char *msg, const char *nickname,
                   int max_size);

void enter_pressed(struct window *win);

void print_char(struct window *win, char c, bool to_refresh);

#endif
