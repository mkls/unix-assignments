#ifndef IPC_H
#define IPC_H

#include "window.h"

struct thread_data {
    struct window *w;
    int fd;
};

void *reader(void *fd);

#endif
