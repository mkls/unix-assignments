#include "ipc.h"

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>

#include "window.h"

#define BUFSIZE 128

void* reader(void* data) {
    struct thread_data* d = (struct thread_data*)data;
    char* buffer = (char*)malloc(sizeof(char) * BUFSIZE);

    int fd = d->fd;
    fd_set readset;

    int err = 0;

    struct timeval tv;
    tv.tv_sec = 1000;
    tv.tv_usec = 0;

    while (1) {
        FD_ZERO(&readset);
        FD_SET(fd, &readset);

        err = select(fd + 1, &readset, NULL, NULL, &tv);

        if (err > 0 && FD_ISSET(fd, &readset)) {
            FD_CLR(fd, &readset);
            int bytes_read = read(fd, buffer, BUFSIZE);
            print_message(d->w, buffer, "other", bytes_read);
        }
    }

    free(buffer);
}
