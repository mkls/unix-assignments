#include "window.h"

#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct window* setup_custom_window(int y, int x, int h, int w) {
    WINDOW* win;
    win = newwin(h, w, y, x);
    box(win, 0, 0);

    struct window* cw = (struct window*)malloc(sizeof(struct window));

    cw->cx = 1;
    cw->cy = 1;
    cw->h = h;
    cw->w = w;
    cw->win = win;

    wmove(cw->win, 1, 1);

    wrefresh(cw->win);

    return cw;
}

void print_message(struct window* win, char* msg, const char* nickname,
                   int max_size) {
    mvwprintw(win->win, win->cy, win->cx, "%s: ", nickname);
    win->cx += strlen(nickname) + 2;

    int i = 0;

    while (*(msg + i) && i < max_size) {
        print_char(win, msg[i++], false);
    }

    win->cx = 1;
    win->cy++;
    // wmove(win->win, ++win->cy, win->cx);
    wrefresh(win->win);
}

void backspace_pressed(struct window* win) {
    if (win->cx != 1) {
        mvwprintw(win->win, win->cy, --win->cx, " ");
        wmove(win->win, win->cy, win->cx);
        wrefresh(win->win);
    }
}

void enter_pressed(struct window* win) {
    wclear(win->win);

    win->cx = 1;
    win->cy = 1;

    wmove(win->win, 1, 1);
    box(win->win, 0, 0);
    wrefresh(win->win);
}

void print_char(struct window* win, char ch, bool to_refresh) {
    mvwprintw(win->win, win->cy, win->cx++, "%c", ch);

    if (win->cx == win->w - 1) {
        win->cx = 1;
        win->cy++;
    }

    if (win->cy == win->h - 1) {
        wclrtoeol(win->win);
        scroll(win->win);
        win->cy--;
        box(win->win, 0, 0);
    }

    if (to_refresh) {
        wrefresh(win->win);
    }
}
