#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_SIZE 128

struct msg_data {
    size_t index;
    size_t len;
    double vec[MAX_SIZE];
    double row[MAX_SIZE];
    double result;
};

struct msg {
    long mtype;
    struct msg_data payload;
};

double** read_matrix(FILE* file, size_t* rows, size_t* cols) {
    double** matrix;

    fscanf(file, "%zu %zu", rows, cols);

    if (*rows != 0 && *cols != 0) {
        matrix = (double**)malloc(sizeof(double*) * (*rows));

        for (size_t i = 0; i < *rows; i++) {
            matrix[i] = (double*)malloc(sizeof(double) * (*cols));

            for (size_t j = 0; j < *cols; j++) {
                fscanf(file, "%lf", *(matrix + i) + j);
            }
        }
    } else {
        matrix = NULL;
    }

    return matrix;
}

double* read_vector(FILE* file, size_t len) {
    double* vec = (double*)malloc(sizeof(double) * len);

    for (size_t i = 0; i < len; i++) {
        fscanf(file, "%lf", vec + i);
    }

    return vec;
}

void print_matrix(double** matrix, size_t rows, size_t cols) {
    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < cols; j++) {
            printf("%f ", matrix[i][j]);
        }
        printf("\n");
    }
}

int main(int argc, char** argv) {
    if (argc != 3) {
        // TODO: call usage()
        puts("Needs exactly 2 arguments: input_file, output_file");
        exit(EXIT_FAILURE);
    }

    char* fname = argv[1];
    char* ofname = argv[2];

    size_t rows, cols;
    int nprocs = get_nprocs();

    printf("%d processors available\n", nprocs);

    int queue_id;
    key_t key = ftok(".", 'm');

    if ((queue_id = msgget(key, IPC_CREAT | 0660)) == -1) {
        perror("msgget()");
        exit(EXIT_FAILURE);
    }

    struct msqid_ds ds;
    msgctl(queue_id, IPC_STAT, &ds);

    printf("Max bytes in queue: %zu\n", ds.msg_qbytes);

    ds.msg_qbytes = 16777216;
    msgctl(queue_id, IPC_SET, &ds);

    printf("New max bytes in queue: %zu\n", ds.msg_qbytes);

    pid_t children[nprocs];
    pid_t pid;

    size_t n;

    for (n = 0; n < nprocs; n++) {
        pid = fork();

        if (pid > 0) {
            children[n] = pid;
        } else if (pid == -1) {
            perror("fork(): ");
            exit(EXIT_FAILURE);
        } else {
            break;
        }
    }

    // Parent process
    if (pid != 0) {
        puts("Reading matrix from input file");

        FILE* f = fopen(fname, "r");

        double** matrix = read_matrix(f, &rows, &cols);
        double* vec = read_vector(f, cols);

        fclose(f);

        puts("Read successfully");

        // print_matrix(matrix, rows, cols);
        // print_matrix(&vec, 1, cols);

        int rows_per_proc = rows / nprocs;
        int remainder = 0;

        if (rows_per_proc == 0) {
            rows_per_proc = 1;
        } else {
            remainder = rows % nprocs;
        }

        puts("Sending matrix rows to child processes");

        for (size_t i = 0, k = 1; i < rows && k <= nprocs;
             i += rows_per_proc, k++) {
            int additional = 1 ? remainder > 0 : 0;

            for (size_t j = 0; j < rows_per_proc + additional && i + j < rows;
                 j++) {
                struct msg_data d;
                struct msg m;

                memcpy(d.row, matrix[i + j], sizeof(double) * cols);
                memcpy(d.vec, vec, sizeof(double) * cols);

                d.len = cols;
                d.index = i + j;

                m.mtype = (long)k;
                m.payload = d;

                printf("Sending row %zu to child %zu\n", i + j, k);
                msgsnd(queue_id, &m, sizeof(struct msg_data), IPC_NOWAIT);
                perror("msgsnd()");
            }

            if (remainder > 0) {
                remainder--;
                i++;
            }
        }

        double* result = (double*)malloc(sizeof(double) * rows);

        puts("Waiting for children");

        for (size_t i = 0; i < rows; i++) {
            struct msg m;
            m.mtype = i + 1 + cols + nprocs;

            printf("Getting result %zu (mtype: %ld)\n", i, m.mtype);
            msgrcv(queue_id, &m, sizeof(struct msg_data), m.mtype, 0);

            result[m.payload.index] = m.payload.result;
        }

        for (size_t i = 0; i < nprocs; i++) {
            printf("Killing child with pid %d\n", children[i]);
            kill(children[i], SIGTERM);
        }

        puts("Deleting the queue");
        msgctl(queue_id, IPC_RMID, 0);

        puts("Writing data to output file");

        FILE* fp = fopen(ofname, "w");

        for (size_t i = 0; i < rows; i++) {
            fprintf(fp, "%lf\n", result[i]);
        }

        fclose(fp);

        puts("Cleanup");

        for (size_t i = 0; i < rows; i++) {
            free(matrix[i]);
        }
        free(matrix);
        free(vec);
    } else {
        // Children
        struct msg data;
        data.mtype = n + 1;

        while (1) {
            msgrcv(queue_id, &data, sizeof(struct msg_data), data.mtype, 0);
            perror("child msgrcv()");
            double result = 0;

            for (size_t k = 0; k < data.payload.len; k++) {
                result += data.payload.row[k] * data.payload.vec[k];
            }

            data.mtype = 1 + data.payload.index + data.payload.len + nprocs;
            data.payload.result = result;

            printf("Sending back mtype %ld\n", data.mtype);
            msgsnd(queue_id, &data, sizeof(struct msg_data), 0);

            data.mtype = (long)n + 1;
        }
    }

    puts("Done!");

    exit(EXIT_SUCCESS);
}
