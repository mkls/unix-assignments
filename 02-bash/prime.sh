#!/usr/bin/env bash

function is_prime() {
	if [[ $# -ne 1 ]];
	then
		echo "is_prime() - argument 'number' is missing."
		exit 1
	fi

	if ! [[ "$1" =~ ^[0-9]+$ ]]
	then
		echo "and it's not an integer number actually. Input must be an integer number"
		exit 2
	fi


	local number=$1
	local base_out="The number $number is"

	for i in $(seq 2 $(echo "$number" | awk 'END { print sqrt($1) }'))
	do
		if [ $(($number % $i)) -eq 0 ]
		then
			echo $base_out "composite :("
			exit 0
		fi
	done

	echo $base_out "prime! Yay!"
}

function main() {
	read -p "Hello! Enter a number to check if it's a prime number!: " number
	echo "Your number is ${number}…"

	is_prime $number
}

main
