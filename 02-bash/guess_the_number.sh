#!/usr/bin/env bash

function gen_random_number() {
	local lower_bound=$1
	local upper_bound=$2
	local rand_number=0

	while [ $rand_number -le $lower_bound ]; do
		rand_number=$(($RANDOM % upper_bound))
	done

	echo "$rand_number"
}

function game_loop() {
	local correct_number=$1

	read -p "Can you guess it in one turn? Enter: " answer

	while [ $answer -ne $correct_number ]; do
		if [ $answer -lt $correct_number ]; then
			echo "That's too little!"
		else
			echo "That's too much!"
		fi

		read -p "Try again: " answer
	done

	echo "Yes! It's $correct_number"
}

function start_game() {
	echo "Let's play a little game."
	echo "Generating a super secret number [1; 100]"

	for i in $(seq 3); do
		printf "%s" "."
		sleep 1
	done

	local rand_number="$(gen_random_number 1 100)"
	printf "\rTry to guess it!\n"

	game_loop $rand_number
}

start_game
